playlist_data = []
playlist_headers = None

def read_playlist_data(filename:str):
    with open(filename) as file:
        for line in file:
            data = line.strip().split(",")
            if playlist_headers is None:
                playlist_headers = data
            else:
                playlist_data.append(data)

def menu_playlists(list_names:list) -> int:
    '''Display a menu of playlist names and return the user's choice.
    list_names: list of playlist names
    return: the user's choice (index in the list)
    '''
    print("Playlists:")

    for i, playlist in enumerate(list_names):
        print(f"{i}: {playlist}")
    while True:
        choice = input("Enter choice: ")
        if choice.isdigit():
            choice = int(choice)
            if choice >= 0 and choice < len(list_names):
                return choice
        print("** invalid choice **")
            

if __name__ == "__main__":
    read_playlist_data("rock_playlist_data.csv")

    # extract the names from the playlist data
    playlists = [x[playlist_headers.index("playlist_name")] for x in playlist_data]

    menu_playlists(playlists)
